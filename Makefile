# Makefil for MyHello pass
#
LEVEL = ../../..

# Path to top level llvm dir
LIBRARYNAME = assignment0-bernales

# Make the shared library become a loadable module so that
# the tools can dlopen/dlsym on the resulting library
LOADABLE_MODULE = 1

# Include the makefile implementation stuff
include $(LEVEL)/Makefile.common

#include "llvm/Pass.h"
#include "llvm/PassAnalysisSupport.h"
#include "llvm/Module.h"
#include "llvm/InstrTypes.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/Dominators.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include <string>
#include <set>

using namespace llvm;

namespace {
    struct FCounterMod : public ModulePass {
        static const std::string EXTERNAL_NODE;
        static const int NA;
        static char ID;
    public:
        FCounterMod() : ModulePass(ID) 
        {
            _funcCount = 0;
            _callEdgeCount = 0;
            _minBB = 0, _maxBB = 0, _countBB = 0;
            _minCFGEdge = 0, _maxCFGEdge = 0, _countCFGEdge = 0;
            _minSingEntryLoop = 0, _maxSingEntryLoop = 0, _countSingEntryLoop = 0;
            _minLoopBB = 0, _maxLoopBB = 0, _countLoopBB = 0;
            _countDominates = 0, _countDominatedBy = 0; 
        }

        virtual bool runOnModule(Module &M) {
            Module::FunctionListType &funcList = M.getFunctionList();
            
            // get function stats which is most of the stats
            getFunctionStats(funcList);
            
            // get call graph edges
            CallGraph &CG = getAnalysis<CallGraph>();
            for (CallGraph::const_iterator cgI = CG.begin(), cgE = CG.end(); cgI != cgE; ++cgI) 
            {
                CallGraphNode &callNode = *(cgI->second);
                _callEdgeCount+= countCallEdges(callNode);
            }
            
            
            // print all of the things
            errs() << "Number of functions: " << _funcCount << "\n";
            errs() << "Number of call edges: " << _callEdgeCount << "\n";
            errs() << "----------------------------\n";
            printStat("Basic Blocks", _countBB/_funcCount, _minBB, _maxBB);
            printStat("Control Flow Edges", _countCFGEdge/_funcCount, _minCFGEdge, _maxCFGEdge);
            printStat("Single Entry Loops", _countSingEntryLoop/_funcCount, _minSingEntryLoop, _maxSingEntryLoop);
            printStat("Loop Basic Blocks", _countLoopBB/_funcCount, _minLoopBB, _maxLoopBB);
            printStat("Basic Blocks Dominating", _countDominates/_funcCount, NA, NA);
            printStat("Basic Blocks Being Dominated", _countDominatedBy/_funcCount, NA, NA);
            return false;
        }
            
        virtual void getAnalysisUsage(AnalysisUsage &AU) const {
            AU.setPreservesAll();
            AU.addRequired<CallGraph>();
            AU.addRequired<LoopInfo>();
            AU.addRequired<DominatorTree>();  
        }
        
    private:
        int _funcCount;
        int _callEdgeCount;
        int _minBB, _maxBB, _countBB;
        int _minCFGEdge, _maxCFGEdge, _countCFGEdge;
        int _minSingEntryLoop, _maxSingEntryLoop, _countSingEntryLoop;
        int _minLoopBB, _maxLoopBB, _countLoopBB;
        int _countDominates, _countDominatedBy; 
        
        // get stats about each function
        void getFunctionStats(Module::FunctionListType &funcList) {
            for (Module::iterator funci = funcList.begin(); funci != funcList.end(); ++funci) 
            {
                Function &f = *funci;
                // having no BB means it's not a function in our applicatoin, it's external
                if(f.getBasicBlockList().size() > 0)
                {
                    _funcCount++;
                    countBB(f);
                    countCFGEdges(f);       
                    countLoops(f);
                    countLoopBBs(f);
                    countBBDom(f);
                }
            }            
        }
        
        // counts dges between functions
        // we are counting incomming possible calls from NULL as a call
        int countCallEdges(CallGraphNode &callNode) {
            
            int edgesCount = 0;
            std::map<std::string, int> funcMap;
            
            for (CallGraphNode::const_iterator cnI = callNode.begin(), cnE = callNode.end(); cnI != cnE; ++cnI) 
            {
                // get the name of the function
                std::string funcName;
                if (Function *FI = cnI->second->getFunction())
                {
                    funcName = FI->getName();
                } else
                {
                    funcName = EXTERNAL_NODE;
                }
                    
                // check if we alread counted a edge to that function
                if (funcMap.count(funcName) == 0)
                {
                    funcMap[funcName] = 1;
                    edgesCount++;
                }
            }
            return edgesCount;
        }
        
        // counts the number of Basic Blocks inside the function
        void countBB(Function &f)
        {
            int numBB = f.getBasicBlockList().size();
            
            // updates stats
            if(_countBB == 0) {
                _minBB = numBB;
                _maxBB = numBB;
            } else if(numBB < _minBB) {
                _minBB = numBB;
            } else if(numBB > _maxBB) {
                _maxBB = numBB;
            }
            
            _countBB+= numBB;
        }
        
        // counts the number of CFG edges of each block
        // this is done by grabbing a terminating instruction and seeing its possible successors
        void countCFGEdges(Function &f)
        {
            const Function::BasicBlockListType &bbList = f.getBasicBlockList();
            int cfgEdgesCount = 0;
            
            for (Function::BasicBlockListType::const_iterator bbI = bbList.begin(), bbE = bbList.end(); bbI != bbE; ++bbI) 
            {
               const TerminatorInst &TI = *bbI->getTerminator();
               cfgEdgesCount+= TI.getNumSuccessors();
            }
            
            // seems like an empty function
            if(cfgEdgesCount == 0) {
                return;
            }
            
            // update stats
            if(_countCFGEdge == 0) {
                _minCFGEdge = cfgEdgesCount;
                _maxCFGEdge = cfgEdgesCount;
            } else if(cfgEdgesCount < _minCFGEdge) {
                _minCFGEdge = cfgEdgesCount;
            } else if(cfgEdgesCount > _maxCFGEdge) {
                _maxCFGEdge = cfgEdgesCount;
            }
            
            _countCFGEdge+= cfgEdgesCount;
        }

        // just ocunts the number of loops inside of a function
        void countLoops(Function &f) 
        {
            SmallVector<std::pair<const BasicBlock*,const BasicBlock*>, 32 > Edges;
            FindFunctionBackedges(f, Edges);
            int loopCount = Edges.size();
            
            // update stats
            if(_countSingEntryLoop == 0) {
                _minSingEntryLoop = loopCount;
                _maxSingEntryLoop = loopCount;
            } else if(loopCount < _minSingEntryLoop) {
                _minSingEntryLoop = loopCount;
            } else if(loopCount > _maxSingEntryLoop) {
                _maxSingEntryLoop = loopCount;
            } 
            
            _countSingEntryLoop+= loopCount;            
        }
        
        // counts the BBs inside each loop, note this counts BB inside nodes only once, If loops are nested, don't matter
        void countLoopBBs(Function &f)
        {
            int loopBBCount = 0; 
            if (!f.isDeclaration())
            {
                LoopInfo &LI = getAnalysis<LoopInfo>(f); 
                     
                for (LoopInfo::iterator liI = LI.begin(), liE = LI.end(); liI != liE; ++liI) 
                {
                    const Loop &L = **liI;
                    loopBBCount+= L.getNumBlocks();
                }         
            }
            
            if(_countLoopBB == 0) {
                _minLoopBB = loopBBCount;
                _maxLoopBB = loopBBCount;
            } else if(loopBBCount < _minLoopBB) {
                _minLoopBB = loopBBCount;
            } else if(loopBBCount > _maxLoopBB) {
                _maxLoopBB = loopBBCount;
            } 
            
            _countLoopBB+= loopBBCount;

        }

        // we are going to count both domniated by and dominates here
        void countBBDom(Function &f)
        {
            DominatorTree& DT = getAnalysis<DominatorTree>(f);
            const Function::BasicBlockListType &bbList = f.getBasicBlockList();
            
            int dominatesCount = 0;
            int dominatedByCount = 0;
            
            // for each BB, add up how many it domniates and how many are domniated by it
            // this is a very slow algorithm, n Squared!!
            for (Function::BasicBlockListType::const_iterator bbI = bbList.begin(), bbE = bbList.end(); bbI != bbE; ++bbI) 
            {   
                for (Function::BasicBlockListType::const_iterator inbbI = bbList.begin(), inbbE = bbList.end(); inbbI != inbbE; ++inbbI) 
                {
              
                    // dominates
                    if(DT.properlyDominates(bbI, inbbI))
                    {
                        dominatesCount++;
                    }
                       
                    // is dominated by
                    if(DT.properlyDominates(inbbI, bbI))
                    {
                        dominatedByCount++;
                    }
                }
            }

            _countDominates+= dominatesCount;/// f.getBasicBlockList().size();
            _countDominatedBy+= dominatedByCount;// / f.getBasicBlockList().size();            
        }
    
        void printStat(const char* stat, int avg, int min, int max)
        {
            errs() << stat <<"\n\t";
            errs() << "avg: " << avg <<", ";
            
            if(min != NA)
                errs() << "min: " << min <<", ";
            else
                errs() << "min: N/A, ";
            
            if(max != NA)
                errs() << "max: " << max <<"\n";
            else
                errs() << "max: N/A\n";
        }
        

    };
}

char FCounterMod::ID = 0;
const int FCounterMod::NA = -1;
const std::string FCounterMod::EXTERNAL_NODE = "external_node__";
static RegisterPass<FCounterMod> X("fcountermod", "Count functions and the edges between them.", false, false);

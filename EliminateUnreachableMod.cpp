#include "llvm/Pass.h"
#include "llvm/PassAnalysisSupport.h"
#include "llvm/Module.h"
#include "llvm/InstrTypes.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/Dominators.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include <string>
#include <set>

using namespace llvm;

namespace {
    struct EliminateUnreachableMod : public FunctionPass {

        static char ID;
    public:
        EliminateUnreachableMod() : FunctionPass(ID) {}

        virtual bool runOnFunction(Function &f) {
            DominatorTree& DT = getAnalysis<DominatorTree>();
            Function::BasicBlockListType &bbList = f.getBasicBlockList();
            int unreachableCount = 0;
            
            for (Function::BasicBlockListType::iterator bbI = bbList.begin(), bbE = bbList.end(); bbI != bbE; ++bbI) 
            {
                if(!DT.isReachableFromEntry(bbI))
                {
                    unreachableCount++;
                    DeleteDeadBlock(bbI);
                    // (*bbI).eraseFromParent();
                }
            }
            
            errs() << "Removed [" << unreachableCount << "] basic blocks from function [" << f.getName() << "]\n";

            return true;
        }
            
        virtual void getAnalysisUsage(AnalysisUsage &AU) const {
            AU.addRequired<DominatorTree>();  
        }
    };
}

char EliminateUnreachableMod::ID = 1;
static RegisterPass<EliminateUnreachableMod> X("eunreachablemod", "eliminate unreachable basic blocks.", false, false);

#include "llvm/Pass.h"
#include "llvm/CallGraphSCCPass.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace {
    struct FunctionCounter : public CallGraphSCCPass {
        
        static char ID;
        FunctionCounter() : CallGraphSCCPass(ID) {}

        
        virtual bool runOnSCC(CallGraphSCC &SCC) {
            errs() << "Serg's Hello: \n";
            
            return false;
        }
    };
}

char FunctionCounter::ID = 0;
static RegisterPass<FunctionCounter> X("functioncount", "Count functions and the edges between them.", false, false);
